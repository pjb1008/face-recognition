//var cr;

$(function() {
    $('#snap').on('click', function(e) {

        const video = document.getElementById('video');
        const canvas = document.createElement('canvas');
        const context = canvas.getContext("2d");
        console.log(video);
        if (video.videoWidth && video.videoHeight) {
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            context.drawImage(video, 0, 0, canvas.width, canvas.height);

            let blob = canvas.toBlob(function(blob) {
                const formData = new FormData();
                formData.append('pic', blob);

                const myFile = new File([blob], 'photo.png', {
                        type: 'image/png',
                        lastModified: new Date(),
                });

                const dataTransfer = new DataTransfer();
                dataTransfer.items.add(myFile);
                document.getElementById('pic').files = dataTransfer.files;
                $('#go').click();

            });
        }
        e.preventDefault();
    });

    $('#pic').on('change', function(e) {$('#go').click()});
    const image = $('#image');
    if (image.length)
    {
        let x = parseInt($('#x').val());
        let y = parseInt($('#y').val());
        let scale = parseFloat($('#scale').val());
        let target_width = parseInt($('#target_width').val());
        let target_height = parseInt($('#target_height').val());

        const cropper = new Cropper(image[0], {
          aspectRatio: target_width / target_height,
          viewMode: 0,
          autoCrop: false,
          cropBoxMovable: false,
          cropBoxResizable: false,
          dragMode: 'move',
          toggleDragModeOnDblclick: false,
          ready() {
            console.log('X scale', scale);
            console.log('X move', x, y);
            cropper.zoomTo(scale, scale);
            cropper.moveTo(x, y);
            cropper.crop();
            cropper.setCropBoxData({left: target_width/2, top: target_height/2, width: target_width, height: target_height });
          },
        });

//         cr = cropper;

        $('#use').on('click', function(e) {
            cropper.getCroppedCanvas({
                fillColor: '#fff',
                imageSmoothingEnabled: true,
                imageSmoothingQuality: 'high',
            }).toBlob(function (blob) {
                const formData = new FormData();
                formData.append('pic', blob);
                $.ajax('cropped', {
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                }).done(function(msg) {
                    console.log(msg);
                    alert(msg);
                }).fail(function( jqXHR, status) {
                    alert(status);
                });
            });
        });

        $('#rotl').on('click', function(e) {cropper.rotate(-1); return true;});
        $('#rotr').on('click', function(e) {cropper.rotate(1); return true;});
        $('#rotr90').on('click', function(e) {cropper.rotate(90); e.preventDefault(); return true;});
    }
});