from typing import Optional

import numpy as np
from flask import Flask, render_template, request
import cv2
from base64 import b64decode, b64encode
import re

app = Flask(__name__)


@app.route('/cropped', methods=['POST'])
def receive_cropped_image():
    pic = request.files.get('pic')
    with open('image.png', 'wb') as f:
        f.write(pic.stream.read())
    return {'ok': True}


@app.route('/', methods=['GET', 'POST'])
def home():
    data: Optional[bytes] = None
    pic_b64: Optional[str] = None
    media_type = None
    face = {}
    if request.method == 'POST' and not request.form.get('restart'):
        pic = request.files.get('pic')
        if pic and pic.mimetype in ('image/jpeg', 'image/png'):
            media_type = pic.mimetype
            data = pic.stream.read()
        elif 'pic2' in request.form:
            extract = re.compile(r'data:([^;]+);base64,(.*)', re.DOTALL)
            if match := extract.fullmatch(request.form['pic2']):
                media_type, data = match.group(1), b64decode(match.group(2))

    target_width = 150
    target_height = 200
    target_aspect = target_width / target_height


    if data:
        face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_alt.xml')
        img = cv2.imdecode(np.asarray(bytearray(data), dtype="uint8"), cv2.IMREAD_COLOR)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.05, 4)
        img_height, img_width = img.shape[0:2]
        if len(faces):
            (x, y, width, height) = list(map(int, faces[0]))

            dx = width * 0.15
            dy = height * 0.15
            x -= dx
            y -= dy
            width += 2*dx
            height += 2*dy

            if width > 30 and height > 30:
                actual_aspect = width / height

                if actual_aspect < target_aspect:  # Tall target area
                    scale = target_height / height
                else:  # Wide target area
                    scale = target_width / width

                x *= scale
                y *= scale
                img_width *= scale
                img_height *= scale
                x = target_width / 2 - x
                y = target_height / 2 - y

                x = min(target_width/2, max(target_width * 1.5 - img_width, x))
                y = min(target_height/2, max(target_height * 1.5 - img_height, y))


                face = dict(x=int(x), y=int(y), scale=scale)
        pic_b64 = f'data:{media_type};base64,{b64encode(data).decode()}'

    return render_template('home.html', pic_b64=pic_b64, face=face, target_width=target_width, target_height=target_height, **face)


if __name__ == '__main__':
    app.run(debug=True)
